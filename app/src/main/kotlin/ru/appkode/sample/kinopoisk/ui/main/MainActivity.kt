package ru.appkode.sample.kinopoisk.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.appkode.sample.kinopoisk.R

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
  }
}
